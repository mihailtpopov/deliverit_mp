﻿using DeliverIT.Data;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ParcelsTests
{
    [TestClass]
    public class ParcelsSortBy_Should
    {
        [TestMethod]
        public void SortBy_ShouldThrowArgumentException_WhenInvalidWeightOrderIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using(var context = new DeliverITDbContext(options))
            {
                var sut = new ParcelService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentException>(() => sut.SortBy("weight+wrongOrder"));
            }
        }

        [TestMethod]
        public void SortBy_ShouldThrowArgumentException_WhenInvalidArrivalDateOrderIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                var sut = new ParcelService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentException>(() => sut.SortBy("arrivalDate+wrongOrder"));
            }
        }

        [TestMethod]
        public void SortBy_ShouldThrowArgumentException_WhenInvalidSortingCriteriaIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                var sut = new ParcelService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentException>(() => sut.SortBy("invalidSortingCriteria"));
            }
        }

        [TestMethod]
        public void SortBy_ShouldReturnSortedInAscendingOrderByWeightWithSpecifiedOrderCollection_WhenValidArgumentsIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sortedByWeight = context.Parcels.OrderBy(p => p.Weight);

                var sut = new ParcelService(context);

                //Act 
                var result = sut.SortBy("weight asc");

                //Assert
                Assert.AreEqual(sortedByWeight.Count(), result.Count());
                Assert.AreEqual(sortedByWeight.First().ParcelId, result.First().Id);
                Assert.AreEqual(sortedByWeight.Last().ParcelId, result.Last().Id);
            }
        }

        [TestMethod]
        public void SortBy_ShouldReturnSortedInAscendingOrderByWeightWithoutSpecifiedOrderCollection_WhenValidArgumentsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sortedByWeight = context.Parcels.OrderBy(p => p.Weight);

                var sut = new ParcelService(context);

                //Act 
                var result = sut.SortBy("weight");

                //Assert
                Assert.AreEqual(sortedByWeight.Count(), result.Count());
                Assert.AreEqual(sortedByWeight.First().ParcelId, result.First().Id);
                Assert.AreEqual(sortedByWeight.Last().ParcelId, result.Last().Id);
            }
        }

        [TestMethod]
        public void SortBy_ShouldReturnSortedInDescendingOrderByWeightCollection_WhenValidArgumentsArePassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sortedByWeight = context.Parcels.OrderByDescending(p => p.Weight);

                var sut = new ParcelService(context);

                //Act 
                var result = sut.SortBy("weight desc");

                //Assert
                Assert.AreEqual(sortedByWeight.Count(), result.Count());
                Assert.AreEqual(sortedByWeight.First().ParcelId, result.First().Id);
                Assert.AreEqual(sortedByWeight.Last().ParcelId, result.Last().Id);
            }
        }

        [TestMethod]
        public void SortBy_ShouldReturnSortedInAscendingOrderByArrivalDateWithoutSpecifiedOrderCollection_WhenValidArgumentsArePassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sortedByWeight = context.Parcels.OrderBy(p => p.Shipment.ArrivalDate);

                var sut = new ParcelService(context);

                //Act 
                var result = sut.SortBy("arrivalDate");

                //Assert
                Assert.AreEqual(sortedByWeight.Count(), result.Count());
                Assert.AreEqual(sortedByWeight.First().ParcelId, result.First().Id);
                Assert.AreEqual(sortedByWeight.Last().ParcelId, result.Last().Id);
            }
        }

        [TestMethod]
        public void SortBy_ShouldReturnSortedInAscendingOrderByArrivalDateWithSpecifiedOrderCollection_WhenValidArgumentsArePassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sortedByWeight = context.Parcels.OrderBy(p => p.Shipment.ArrivalDate);

                var sut = new ParcelService(context);

                //Act 
                var result = sut.SortBy("arrivalDate asc");

                //Assert
                Assert.AreEqual(sortedByWeight.Count(), result.Count());
                Assert.AreEqual(sortedByWeight.First().ParcelId, result.First().Id);
                Assert.AreEqual(sortedByWeight.Last().ParcelId, result.Last().Id);
            }
        }

        [TestMethod]
        public void SortBy_ShouldReturnSortedInDescendingOrderByArrivalDateOrderCollection_WhenValidArgumentsArePassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sortedByWeight = context.Parcels.OrderByDescending(p => p.Shipment.ArrivalDate);

                var sut = new ParcelService(context);

                //Act 
                var result = sut.SortBy("arrivalDate desc");

                //Assert
                Assert.AreEqual(sortedByWeight.Count(), result.Count());
                Assert.AreEqual(sortedByWeight.First().ParcelId, result.First().Id);
                Assert.AreEqual(sortedByWeight.Last().ParcelId, result.Last().Id);
            }
        }

        [TestMethod]
        public void SortBy_ShouldReturnSortedInAscendingOrderByWeightAndArrivalDateOrderCollection_WhenValidArgumentsArePassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sortedByWeight = context.Parcels
                    .OrderBy(p => p.Weight)
                    .ThenBy(p=>p.Shipment.ArrivalDate);

                var sut = new ParcelService(context);

                //Act 
                var result = sut.SortBy("weight,arrivalDate");

                //Assert
                Assert.AreEqual(sortedByWeight.Count(), result.Count());
                Assert.AreEqual(sortedByWeight.First().ParcelId, result.First().Id);
                Assert.AreEqual(sortedByWeight.Last().ParcelId, result.Last().Id);
            }
        }

        [TestMethod]
        public void SortBy_ThrowsArgumentException_WhenLargerNumberOfSortingCriteriasIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                var sut = new ParcelService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentException>(() => sut.SortBy("weight,arrivalDate,wrongCriteria"));
            }
        }

        [TestMethod]
        public void SortBy_ThrowsArgumentException_SomethingMoreIsPassedRatherThanCriteriaAndOrder()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                var sut = new ParcelService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentException>(() => sut.SortBy("weight asc somethingWrong"));
            }
        }
    }
}
