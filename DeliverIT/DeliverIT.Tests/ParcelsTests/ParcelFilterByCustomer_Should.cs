﻿using DeliverIT.Data;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ParcelsTests
{
    [TestClass]
    public class ParcelFilterByCustomer_Should
    {
        [TestMethod]
        public void FilterByCustomer_ShouldReturnAllCustomers_WhenNullArgumentsArePassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var allParcels = context.Parcels.ToList();

                var sut = new ParcelService(context);

                //Act
                var result = sut.FilterByCustomer(null, null);

                //Assert
                Assert.AreEqual(allParcels.Count(), result.Count());
                Assert.AreEqual(allParcels.First().ParcelId, result.First().Id);
                Assert.AreEqual(allParcels.Last().ParcelId, result.Last().Id);
            }
        }

        [TestMethod]
        public void FilterByCustomer_ShouldReturnEmptyCollection_WhenInvalidArgumentsArePassed()
        {
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                //Act
                var parcels = sut.FilterByCustomer("testFirstName", "testLastName");

                //Assert
                Assert.IsFalse(parcels.Any());
            }
        }

        [TestMethod]
        public void FilterByCustomer_ShouldReturnFilteredByFirstNameCollection_WhenValidFirstNameIsPassed()
        {
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var parcels = context.Parcels.Where(p => p.Purchaser.FirstName == "Ivan");

                var sut = new ParcelService(context);

                //Act
                var result = sut.FilterByCustomer("Ivan", null);

                //Assert
                Assert.AreEqual(parcels.Count(), result.Count());
                Assert.AreEqual(parcels.First().ParcelId, result.First().Id);
                Assert.AreEqual(parcels.Last().ParcelId, result.Last().Id);
            }
        }

        [TestMethod]
        public void FilterByCustomer_ShouldReturnFilteredByLastNameCollection_WhenValidLastNameIsPassed()
        {
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var parcels = context.Parcels.Where(p => p.Purchaser.LastName == "Kolev");

                var sut = new ParcelService(context);

                //Act
                var result = sut.FilterByCustomer(null, "Kolev");

                //Assert
                Assert.AreEqual(parcels.Count(), result.Count());
                Assert.AreEqual(parcels.First().ParcelId, result.First().Id);
                Assert.AreEqual(parcels.Last().ParcelId, result.Last().Id);
            }
        }

        [TestMethod]
        public void FilterByCustomer_ShouldReturnFilteredByLastAndFirstNameCollection_WhenValidLastAndFirstNameArePassed()
        {
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var parcels = context.Parcels
                    .Where(p => p.Purchaser.LastName == "Kolev" && p.Purchaser.FirstName == "Ivan");

                var sut = new ParcelService(context);

                //Act
                var result = sut.FilterByCustomer("Ivan", "Kolev");

                //Assert
                Assert.AreEqual(parcels.Count(), result.Count());
                Assert.AreEqual(parcels.First().ParcelId, result.First().Id);
                Assert.AreEqual(parcels.Last().ParcelId, result.Last().Id);
            }
        }
    }
}
