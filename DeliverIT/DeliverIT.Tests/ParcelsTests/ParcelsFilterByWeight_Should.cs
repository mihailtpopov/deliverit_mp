﻿using DeliverIT.Data;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ParcelsTests
{
    [TestClass]
    public class ParcelsFilterByWeight_Should
    {
        [TestMethod]
        public void FilterByWeight_ShouldThrowArgumentException_WhenNegativeStartingWeightIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using(var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentException>(() => sut.FilterByWeight(3, -1));
            }
        }

        [TestMethod]
        public void FilterByWeight_ShouldThrowArgumentException_WhenNegativeEndingWeightIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentException>(() => sut.FilterByWeight(-1, 3));
            }
        }

        [TestMethod]
        public void FilterByWeight_ShouldReturnCorrectlyFilteredByWeightCollection_WhenValidArgumentsArePassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var filteredParcels = context.Parcels.Where(p => p.Weight >= 1 && p.Weight <= 5);

                var sut = new ParcelService(context);

                //Act
                var result = sut.FilterByWeight(5, 1);

                //Assert
                Assert.AreEqual(filteredParcels.Count(), result.Count());
                Assert.AreEqual(filteredParcels.First().ParcelId, result.First().Id);
                Assert.AreEqual(filteredParcels.Last().ParcelId, result.Last().Id);
            }
        }
    }
}
