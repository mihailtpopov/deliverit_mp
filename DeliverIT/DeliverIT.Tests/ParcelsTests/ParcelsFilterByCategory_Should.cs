﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ParcelsTests
{
    [TestClass]
    public class ParcelsFilterByCategory_Should
    {
        [TestMethod]
        public void FilterByCategory_ShouldThrowCategoryNotFoundException_WhenInvalidCategoryIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using(var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                //Act && Assert
                Assert.ThrowsException<CategoryNotFoundException>(() => sut.FilterByCategory("invalidCategory"));
            }
        }

        [TestMethod]
        public void FilterByCategory_ShouldReturnRightCollection_WhenValidCategoryIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var filteredParcels = context.Parcels.Where(p => p.Category.Name == "finished");

                var sut = new ParcelService(context);

                //Act
                var result = sut.FilterByCategory("finished");

                //Assert
                Assert.AreEqual(filteredParcels.Count(), result.Count());
                Assert.AreEqual(filteredParcels.First().ParcelId, result.First().Id);
                Assert.AreEqual(filteredParcels.Last().ParcelId, result.Last().Id);
            }
        }
    }
}
