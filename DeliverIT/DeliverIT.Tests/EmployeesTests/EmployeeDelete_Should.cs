﻿using DeliverIT.Data;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.EmployeesTests
{
    [TestClass]
    public class EmployeeDelete_Should
    {
        [TestMethod]
        public void Delete_ShouldReturnFalse_WhenInvalidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using(var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var invalidId = context.Employees.Max(e => e.EmployeeId) + 1;

                //Act && Assert
                Assert.AreEqual(false, sut.Delete(invalidId));
            }
        }

        [TestMethod]
        public void Delete_ShouldReturnTrue_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var validId = context.Employees.Max(e => e.EmployeeId);

                //Act && Assert
                Assert.AreEqual(true, sut.Delete(validId));
            }
        }

        [TestMethod]
        public void Delete_ShouldSuccessfullyRemoveInstance_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var validId = context.Employees.Max(e => e.EmployeeId);

                var count = context.Employees.Count();

                //Act
                _ = sut.Delete(validId);

                //Assert
                Assert.AreEqual(context.Employees.Count(), count - 1);
                Assert.IsTrue(context.Employees.FirstOrDefault(e => e.EmployeeId == validId) == null);
            }
        }
    }
}
