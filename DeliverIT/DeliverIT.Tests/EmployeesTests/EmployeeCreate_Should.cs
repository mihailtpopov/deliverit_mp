﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.EmployeesTests
{
    [TestClass]
    public class EmployeeCreate_Should
    {
        [TestMethod]
        public void Create_ShouldThrowArgumentNullException_WhenNullInputModelIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentNullException>(() => sut.Create(null));
            }
        }

        [TestMethod]
        public void Create_ShouldThrowAddressNotFoundException_WhenInvalidInputModelAddressIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var inputModel = new InputEmployee()
                {
                    AddressId = context.Addresses.Max(a => a.AddressId) + 1,
                    Email = "validemail@gmail.com",
                    FirstName = "validFirstName",
                    LastName = "validLastName"
                };

                //Act && Assert
                Assert.ThrowsException<AddressNotFoundException>(() => sut.Create(inputModel));
            }
        }

        [TestMethod]
        public void Create_ShouldThrowDuplicateEmailAddressException_WhenExistingInputModelEmailIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var inputModel = new InputEmployee()
                {
                    AddressId = context.Employees.Max(e => e.AddressId),
                    Email = "atanaskolev@gmail.com",
                    FirstName = "validFirstName",
                    LastName = "validLastName"
                };

                //Act && Assert
                Assert.ThrowsException<DuplicateEmailAddressException>(() => sut.Create(inputModel));
            }
        }

        [TestMethod]
        public void Create_ShouldThrowArgumentNullException_WhenNullInputModelAddressIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var inputModel = new InputEmployee()
                {
                    AddressId = null,
                    Email = "validEmail@gmail.com",
                    FirstName = "validFirstName",
                    LastName = "validLastName"
                };

                //Act && Assert
                Assert.ThrowsException<ArgumentNullException>(() => sut.Create(inputModel));
            }
        }

        [TestMethod]
        public void Create_ShouldThrowArgumentNullException_WhenNullInputModelEmailAddressIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var inputModel = new InputEmployee()
                {
                    AddressId = context.Employees.Max(e => e.AddressId),
                    Email = null,
                    FirstName = "validFirstName",
                    LastName = "validLastName"
                };

                //Act && Assert
                Assert.ThrowsException<ArgumentNullException>(() => sut.Create(inputModel));
            }
        }

        [TestMethod]
        public void Create_ShouldThrowArgumentNullException_WhenNullInputModelFirstNameIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var inputModel = new InputEmployee()
                {
                    AddressId = context.Employees.Max(e => e.AddressId),
                    Email = "validEmail@gmail.com",
                    FirstName = null,
                    LastName = "validLastName"
                };

                //Act && Assert
                Assert.ThrowsException<ArgumentNullException>(() => sut.Create(inputModel));
            }
        }

        [TestMethod]
        public void Create_ShouldThrowArgumentNullException_WhenNullInputModelLastNameIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var inputModel = new InputEmployee()
                {
                    AddressId = context.Employees.Max(e => e.AddressId),
                    Email = "validEmail@gmail.com",
                    FirstName = "validFirstName",
                    LastName = null
                };

                //Act && Assert
                Assert.ThrowsException<ArgumentNullException>(() => sut.Create(inputModel));
            }
        }

        [TestMethod]
        public void Create_ShouldReturnCorrectCreatedInstance_WhenValidInputModelIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var inputModel = new InputEmployee()
                {
                    AddressId = context.Employees.Max(e => e.AddressId),
                    Email = "validEmail@gmail.com",
                    FirstName = "validFirstName",
                    LastName = "validLastName"
                };

                var address = context.Addresses.FirstOrDefault(a => a.AddressId == inputModel.AddressId.Value);
                var addressStreet = address.Street;
                var addressCity = address.City.Name;
                var addressCountry = address.City.Country.Name;

                var employee = new Employee()
                {
                    Address = new Address()
                    {
                        AddressId = inputModel.AddressId.Value,
                        Street = addressStreet,
                        City = new City() 
                        { 
                            Name = addressCity, 
                            Country = new Country() { Name = addressCountry }
                        }
                    },
                    FirstName = inputModel.FirstName,
                    LastName = inputModel.LastName,
                    Email = inputModel.Email
                };

                var employeeDTO = new EmployeeDTO(employee);
                //Act
                var result = sut.Create(inputModel);

                //Assert
                Assert.AreEqual(employeeDTO.Email, result.Email);
                Assert.AreEqual(employeeDTO.Address, result.Address);
                Assert.AreEqual(employeeDTO.FullName, result.FullName);
            }
        }

        [TestMethod]
        public void Create_ShouldSuccessfullyAddInstanceToDatabase_WhenValidInputModelIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new EmployeeService(context);

                var inputModel = new InputEmployee()
                {
                    AddressId = context.Employees.Max(e => e.AddressId),
                    Email = "validEmail@gmail.com",
                    FirstName = "validFirstName",
                    LastName = "validLastName"
                };

                var address = context.Addresses.FirstOrDefault(a => a.AddressId == inputModel.AddressId.Value);
                var addressStreet = address.Street;
                var addressCity = address.City.Name;
                var addressCountry = address.City.Country.Name;

                var employee = new Employee()
                {
                    Address = new Address()
                    {
                        AddressId = inputModel.AddressId.Value,
                        Street = addressStreet,
                        City = new City()
                        {
                            Name = addressCity,
                            Country = new Country() { Name = addressCountry }
                        }
                    },
                    FirstName = inputModel.FirstName,
                    LastName = inputModel.LastName,
                    Email = inputModel.Email
                };

                var employeeDTO = new EmployeeDTO(employee);

                var count = context.Employees.Count();

                //Act
                var result = sut.Create(inputModel);

                //Assert
                Assert.AreEqual(context.Employees.Count(), count + 1);
            }
        }
    }
}
