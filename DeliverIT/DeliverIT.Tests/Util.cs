﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions.Messages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests
{
    public static class Util
    {
        public static DbContextOptions GetDbContextOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<DeliverITDbContext>()
                .UseInMemoryDatabase(databaseName).Options;
        }

        internal static object GetDbContextOptions(object strings)
        {
            throw new NotImplementedException();
        }

        //public static ICollection<Warehouse> GetWarehouses(DeliverITDbContext context)
        //{
        //    return GetLoadedWarehouses(context);
        //}

        //public static ICollection<Address> GetAdresses(DeliverITDbContext context)
        //{
        //    return GetLoadedAddresses(context);
        //}

        //public static ICollection<Customer> GetCustomers(DeliverITDbContext context)
        //{
        //    return GetLoadedCustomers(context);
        //}

        //public static ICollection<Employee> GetEmployees(DeliverITDbContext context)
        //{
        //    return GetLoadedEmployees(context);
        //}

        //public static ICollection<Country> GetCountries(DeliverITDbContext context)
        //{
        //    return GetLoadedCountries(context);
        //}

        //public static ICollection<City> GetCities(DeliverITDbContext context)
        //{
        //    return GetLoadedCities(context);
        //}

        //public static ICollection<Parcel> GetParcels(DeliverITDbContext context)
        //{
        //    return GetLoadedParcels(context);
        //}

        //public static ICollection<Shipment> GetShipments(DeliverITDbContext context)
        //{
        //    return GetLoadedShipments(context);
        //}

        internal static ICollection<ShipmentStatus> SeedShipmentStatuses(DeliverITDbContext context)
        {
            var shipmentStatuses = new List<ShipmentStatus>()
            {
                new ShipmentStatus(){ ShipmentStatusId = 1, Status = "preparing"},
                new ShipmentStatus(){ ShipmentStatusId = 2, Status = "on the way"},
                new ShipmentStatus(){ ShipmentStatusId = 3, Status = "completed"}
            };

            return shipmentStatuses;
        }

        internal static ICollection<Country> SeedCountries(DeliverITDbContext context)
        {
            var countries = new List<Country>()
            {
                new Country(){CountryId = 1, Name = "Bulgaria" },
                new Country(){CountryId = 2, Name = "United Kingdom"},
                new Country(){CountryId = 3, Name = "Germany"},
                new Country(){CountryId = 4, Name =  "Greece" }

            };

            return countries;
        }

        internal static ICollection<City> SeedCities(DeliverITDbContext context)
        {
            var cities = new List<City>()
            {
                new City(){CityId = 1, Name = "Sofia", CountryId = 1},
                new City(){CityId = 2, Name = "London", CountryId = 2},
                new City(){CityId = 3, Name = "Berlin", CountryId = 3},
                new City(){CityId = 4, Name = "Varna", CountryId = 1},
                new City(){CityId = 5, Name = "Manchester", CountryId = 2},
                new City(){CityId = 6, Name = "Hamburg", CountryId = 3}
            };

            return cities;
        }

        internal static ICollection<Address> SeedAddresses(DeliverITDbContext context)
        {
            var addresses = new List<Address>()
            {
                new Address(){AddressId = 1, Street = "bul. Botevgradsko shose", CityId = 1},
                new Address(){AddressId = 2, Street = "bul. Primorski", CityId = 4},
                new Address(){AddressId = 3, Street = "Abbey Road", CityId = 2},
                new Address(){AddressId = 4, Street = "Cheetham Hill Road", CityId = 5},
                new Address(){AddressId = 5, Street = "Tauentzienstrasse", CityId = 3},
                new Address(){AddressId = 6, Street = "Deichstrasse", CityId = 6},
            };

            return addresses;
        }



        internal static ICollection<Customer> SeedCustomers(DeliverITDbContext context)
        {
            var customers = new List<Customer>()
            {
              new Customer() { CustomerId = 1, FirstName = "Dragan", LastName = "Ivanov", AddressId = 2, Email = "draganivanov@gmail.com" },
              new Customer() { CustomerId = 2, FirstName = "Ivan", LastName = "Kolev", AddressId = 1, Email = "ivankolev@gmail.com" },
              new Customer() { CustomerId = 3, FirstName = "John", LastName = "Atanasov", AddressId = 4, Email = "johnatanasov@gmail.com"}
            };

            return customers;
        }

        internal static ICollection<Employee> SeedEmployees(DeliverITDbContext context)
        {
            var employees = new List<Employee>()
            {
                new Employee() { EmployeeId = 1, FirstName = "Atanas", LastName = "Kolev", AddressId = 1, Email = "atanaskolev@gmail.com"},
                new Employee() { EmployeeId = 2, FirstName = "Kolio", LastName = "Manev", AddressId = 1, Email = "koliomanev@gmail.com"},
                new Employee() { EmployeeId = 3, FirstName = "Petar", LastName = "Andreev", AddressId = 1, Email = "petarandreev@gmail.com"}
            };

            return employees;
        }

        internal static ICollection<Parcel> SeedParcels(DeliverITDbContext context)
        {
            var parcels = new List<Parcel>()
            {
                new Parcel() {ShipmentId = 1, ParcelId = 1, CategoryId = 1, CustomerId = 1, WarehouseId= 1, Weight = 6},
                new Parcel() {ShipmentId = 1, ParcelId = 2, CategoryId = 2, CustomerId = 3, WarehouseId= 1, Weight = 4},
                new Parcel() {ShipmentId = 1, ParcelId = 3, CategoryId = 2, CustomerId = 2, WarehouseId= 1, Weight = 5},
                new Parcel() {ShipmentId = 1, ParcelId = 4, CategoryId = 1, CustomerId = 1, WarehouseId= 2, Weight = 10},
            };

            return parcels;
        }

        internal static ICollection<Category> SeedCategories(DeliverITDbContext context)
        {
            var categories =
             new List<Category>()
             {
                new Category() {CategoryId = 1, Name = "in progress"},
                new Category() {CategoryId = 2, Name = "finished" }
             };

            return categories;

        }

        internal static ICollection<Warehouse> SeedWarehouses(DeliverITDbContext context)
        {
            var warehouses = new List<Warehouse>()
            {
                new Warehouse() { WarehouseId = 1, AddressId = 1},
                new Warehouse() { WarehouseId = 2, AddressId = 3},
                new Warehouse() { WarehouseId = 3, AddressId = 5},
                new Warehouse() { WarehouseId = 4, AddressId = 2},
            };

            return warehouses;
        }

        internal static ICollection<Shipment> SeedShipments(DeliverITDbContext context)
        {
            var shipments = new List<Shipment>()
            {
                new Shipment()
                {
                    ShipmentId = 1,
                    DepartureDate = DateTime.Now.AddDays(1),
                    ArrivalDate = DateTime.Now.AddDays(5),
                    WarehouseId = 1,
                    ShipmentStatusId = 1
                },
                new Shipment()
                {
                    ShipmentId = 2,
                    DepartureDate = DateTime.Now.AddDays(1),
                    ArrivalDate = DateTime.Now.AddDays(5),
                    WarehouseId = 1,
                    ShipmentStatusId = 1
                },
                new Shipment()
                {
                    ShipmentId = 3,
                    DepartureDate = DateTime.Now.AddDays(1),
                    ArrivalDate = DateTime.Now.AddDays(5),
                    WarehouseId = 2,
                    ShipmentStatusId = 1
                }
            };

            return shipments;
        }

        internal static void SeedDataInMemoryDatabase(DeliverITDbContext context)
        {
            context.Database.EnsureDeleted();

            context.AddRange(SeedShipmentStatuses(context));
            context.AddRange(SeedShipments(context));
            context.AddRange(SeedWarehouses(context));
            context.AddRange(SeedCategories(context));
            context.AddRange(SeedParcels(context));
            context.AddRange(SeedEmployees(context));
            context.AddRange(SeedAddresses(context));
            context.AddRange(SeedCities(context));
            context.AddRange(SeedCountries(context));
            context.AddRange(SeedCustomers(context));

            context.SaveChanges();
        }


        //    private static ICollection<City> GetLoadedCities(DeliverITDbContext context)
        //    {
        //        return context.Cities.Include(c => c.Country).ToList();
        //    }

        //    private static ICollection<Country> GetLoadedCountries(DeliverITDbContext context)
        //    {
        //        var countries = context.Countries.Include(c => c.Cities);

        //        return countries.ToList();
        //    }

        //    private static ICollection<Customer> GetLoadedCustomers(DeliverITDbContext context)
        //    {
        //        var customers = context.Customers
        //            .Include(c => c.Address)
        //               .ThenInclude(a => a.City)
        //                  .ThenInclude(c => c.Country);

        //        return customers.ToList();
        //    }

        //    private static ICollection<Employee> GetLoadedEmployees(DeliverITDbContext context)
        //    {
        //        var employees = context.Employees
        //            .Include(e => e.Address)
        //               .ThenInclude(a => a.City)
        //                  .ThenInclude(c => c.Country);

        //        return employees.ToList();
        //    }

        //    private static ICollection<Parcel> GetLoadedParcels(DeliverITDbContext context)
        //    {
        //        IEnumerable<Parcel> parcels = context.Parcels
        //            .Include(p => p.Purchaser)
        //            .Include(p => p.Shipment)
        //            .Include(p => p.Warehouse)
        //               .ThenInclude(w => w.Address)
        //                  .ThenInclude(a => a.City)
        //                     .ThenInclude(c => c.Country)
        //            .Include(p => p.Category);

        //        return parcels.ToList();
        //    }

        //    private static ICollection<Shipment> GetLoadedShipments(DeliverITDbContext context)
        //    {
        //        return context.Shipments
        //            .Include(sh => sh.ShipmentStatus)
        //            .Include(sh => sh.Warehouse)
        //                .ThenInclude(w => w.Address)
        //                    .ThenInclude(a => a.City)
        //                        .ThenInclude(c => c.Country)
        //            .Include(sh => sh.Parcels).ToList();
        //    }

        //    private static ICollection<Warehouse> GetLoadedWarehouses(DeliverITDbContext context)
        //    {
        //        return context.Warehouses
        //            .Include(w => w.Address)
        //               .ThenInclude(a => a.City)
        //                  .ThenInclude(c => c.Country)
        //             .Include(w => w.Employees).ToList();
        //    }

        //    private static ICollection<Address> GetLoadedAddresses(DeliverITDbContext context)
        //    {
        //        return context.Addresses
        //            .Include(a => a.City)
        //            .Include(a => a.Customers)
        //            .Include(a => a.Employees)
        //            .ToList();
        //    }
        //}
    }
}
