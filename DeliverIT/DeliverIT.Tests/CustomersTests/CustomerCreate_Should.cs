﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.CustomersTests
{
    [TestClass]
    public class CustomerCreate_Should
    {
        [TestMethod]
        public void Create_ShouldThrowArgumentException_WhenNullParameterIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                var customerService = new CustomerService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentException>(() => customerService.Create(null));
            }
        }

        [TestMethod]
        public void Update_ShouldThrowAddressNotFoundException_WhenInvalidAddressIdIsUpdated()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customer = context.Customers.First();

                var customerService = new CustomerService(context);

                var inputCustomer = new InputCustomer();
                inputCustomer.AddressId = -1;

                //Act && Assert
                Assert.ThrowsException<AddressNotFoundException>(() => customerService.Create(inputCustomer));
            }
        }

        [TestMethod]
        public void Create_ShouldThrowDuplicateEmailAddressException_WhenRegisteredEmailIsUpdated()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customer = context.Customers.First();

                var customerService = new CustomerService(context);

                var inputCustomer = new InputCustomer();
                inputCustomer.Email = "johnatanasov@gmail.com";

                //Act && Assert
                Assert.ThrowsException<DuplicateEmailAddressException>(() => customerService.Create(inputCustomer));
            }
        }

        [TestMethod]
        public void Create_ShouldSuccessfullyAddCustomer_WhenValidParametersArePassed()
        {
            //Arrange 
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customerService = new CustomerService(context);

                var customersCount = context.Customers.Count();

                var inputCustomer = new InputCustomer()
                {
                    Email = "newemail@gmail.com",
                    FirstName = "newFirstName",
                    LastName = "newLastName",
                    AddressId = 1
                };

                //Act 
                customerService.Create(inputCustomer);

                //Assert
                Assert.AreEqual(customersCount + 1, context.Customers.Count());
            }
        }

        [TestMethod]
        public void Create_ShouldReturnCorrectInstanceType_WhenValidParametersArePassed()
        {
            //Arrange 
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customerService = new CustomerService(context);

                var customersCount = context.Customers.Count();

                var inputCustomer = new InputCustomer()
                {
                    Email = "newemail@gmail.com",
                    FirstName = "newFirstName",
                    LastName = "newLastName",
                    AddressId = 1
                };


                //Act 
                var customerDTO = customerService.Create(inputCustomer);

                //Assert
                Assert.IsInstanceOfType(customerDTO, typeof(CustomerDTO));
            }
        }

        [TestMethod]
        public void Create_ShouldReturnCorrectInstance_WhenValidParametersArePassed()
        {
            //Arrange 
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customerService = new CustomerService(context);

                var customersCount = context.Customers.Count();

                var inputCustomer = new InputCustomer()
                {
                    Email = "newemail@gmail.com",
                    FirstName = "newFirstName",
                    LastName = "newLastName",
                    AddressId = 1
                };

                //Act 
                var createdCustomerDTO = customerService.Create(inputCustomer);

                //Assert
                Assert.AreEqual(createdCustomerDTO.Email, inputCustomer.Email);
                
            }
        }
    }
}
