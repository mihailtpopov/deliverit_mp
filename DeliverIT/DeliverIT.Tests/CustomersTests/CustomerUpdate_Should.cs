﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.CustomersTests
{

    [TestClass]
    public class CustomerUpdate_Should
    {
        [TestMethod]
        public void Update_ShouldArgumentNullException_WhenInvalidInputModelIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);
            
            using(var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customer = context.Customers.First();

                var customerService = new CustomerService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentNullException>(() => customerService.Update(customer, null));
            }
        }

        [TestMethod]
        public void Update_ShouldThrowAddressNotFoundException_WhenInvalidAddressIdIsUpdated()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customer = context.Customers.First();

                var customerService = new CustomerService(context);

                var inputCustomer = new InputCustomer();
                inputCustomer.AddressId = -1;

                //Act && Assert
                Assert.ThrowsException<AddressNotFoundException>(() => customerService.Update(customer, inputCustomer));
            }
        }

        [TestMethod]
        public void Update_ShouldThrowDuplicateEmailAddressException_WhenRegisteredEmailIsUpdated()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customer = context.Customers.First();

                var customerService = new CustomerService(context);

                var inputCustomer = new InputCustomer();
                inputCustomer.Email = "johnatanasov@gmail.com";

                //Act && Assert
                Assert.ThrowsException<DuplicateEmailAddressException>(() => customerService.Update(customer, inputCustomer));
            }
        }

        [TestMethod]
        public void Update_ShouldReturnCorrectInstance_WhenValidParametersArePassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customer = context.Customers.First();

                var customerService = new CustomerService(context);

                var inputCustomer = new InputCustomer();
                inputCustomer.Email = "newemail@gmail.com";

                var updatedCustomer = customerService.Update(customer, inputCustomer);

                //Act && Assert
                Assert.AreEqual(updatedCustomer.Email, inputCustomer.Email);
            }
        }

        [TestMethod]
        public void Update_ShouldReturnCorrectInstanceType_WhenValidParametersArePassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customer = context.Customers.First();

                var customerService = new CustomerService(context);

                var inputCustomer = new InputCustomer();
                inputCustomer.Email = "newemail@gmail.com";

                var updatedCustomer = customerService.Update(customer, inputCustomer);

                //Act && Assert
                Assert.IsInstanceOfType(updatedCustomer, typeof(CustomerDTO));
            }
        }
    }
}
