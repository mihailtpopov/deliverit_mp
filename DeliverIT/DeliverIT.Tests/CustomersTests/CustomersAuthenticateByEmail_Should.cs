﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Tests.CustomersTests
{
    [TestClass]
    public class CustomersAuthenticateByEmail_Should
    {
        [TestMethod]
        public void AuthenticateByEmail_ShouldThrowUnauthorizedAccessException_WhenInvalidEmailIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using(var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);
                var wrongEmail = "wrongEmail";
                var sut = new CustomerService(context);

                //Act && Assert
                Assert.ThrowsException<UnauthorizedAccessException>(() => sut.AuthenticateByEmail(wrongEmail));
            }
        }

        [TestMethod]
        public void AuthenticateByEmail_ShouldReturnCorrectCustomerInstance_WhenValidEmailIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);
                var validEmail = "johnatanasov@gmail.com";
                var sut = new CustomerService(context);

                //Act 
                var result = sut.AuthenticateByEmail(validEmail);

                //Assert
                Assert.IsTrue(validEmail == result.Email);
            }
        }

        [TestMethod]
        public void AuthenticateByEmail_ShouldReturnCorrectInstanceType_WhenValidEmailIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);
                var validEmail = "johnatanasov@gmail.com";
                var sut = new CustomerService(context);

                //Act 
                var result = sut.AuthenticateByEmail(validEmail);

                //Assert
                Assert.IsInstanceOfType(result, typeof(Customer));
            }
        }
    }
}
