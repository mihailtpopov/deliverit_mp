﻿using DeliverIT.Data;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.InputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Tests.ShipmentsTests
{
    [TestClass]
    public class ShipmentsCreate_Should
    {
        [TestMethod]
        public void Create_ShouldThrow_WhenNullIsPassed()
        {
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                //Act && Assert
                Assert.ThrowsException<ArgumentException>(() => shipmentService.Create(null));
            }
        }

        [TestMethod]
        public void Create_ShouldThrow_WhenInvalidDatesArePassed()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                // 1. Departure date is after arrival date
                // 2. Departure date is before Now
                // 3. Arrival date is before Now
                List<DateTime> departureDates = new List<DateTime>()
                {
                   DateTime.Now.AddDays(3),
                   DateTime.Now.AddDays(-1),
                   DateTime.Now.AddDays(3)
                };
                List<DateTime> arrivalDates = new List<DateTime>()
                {
                   DateTime.Now.AddDays(2),
                   DateTime.Now.AddDays(2),
                   DateTime.Now.AddDays(-1),
                };

                for (int i = 0; i < arrivalDates.Count; i++)
                {
                    InputShipment input = new InputShipment()
                    {
                        ArrivalDate = arrivalDates[i],
                        DepartureDate = departureDates[i]
                    };

                    // Act & Assert
                    Assert.ThrowsException<ArgumentException>(() => shipmentService.Create(input));
                }
            }
        }

        [TestMethod]
        public void Create_ShouldThrow_WhenInvalidWarehouseIdIsPassed()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                InputShipment input = new InputShipment()
                {
                    WarehouseId = 10
                };

                // Act & Assert
                Assert.ThrowsException<ArgumentException>(() => shipmentService.Create(input));
            }
        }

        [TestMethod]
        public void Create_ShouldReturnCorrectObject_WhenValidInput()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                Util.SeedDataInMemoryDatabase(context);

                InputShipment input = new InputShipment()
                {
                    DepartureDate = DateTime.Now.AddDays(1),
                    ArrivalDate = DateTime.Now.AddDays(2),
                    WarehouseId = 1
                };

                // Act
                var newShipmentDTO = shipmentService.Create(input);

                // Assert
                Assert.AreEqual(input.ArrivalDate.Value.ToString("yyyy/MM/dd"), newShipmentDTO.ArrivalDate);
                Assert.AreEqual(input.DepartureDate.Value.ToString("yyyy/MM/dd"), newShipmentDTO.DepartureDate);
                Assert.AreEqual("preparing", newShipmentDTO.Status);
                Assert.AreEqual(input.WarehouseId, newShipmentDTO.DestinationWarehouse.Id);
            }
        }
    }
}
