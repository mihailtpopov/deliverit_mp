﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.QueryObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ShipmentsTests
{
    [TestClass]
    public class ShipmentFilterByWarehouse_Should
    {
        [TestMethod]
        public void FilterByWarehouse_ShouldThrow_WhenInvalidWarehouseId()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                // Act && Assert

                Assert.ThrowsException<WarehouseNotFoundException>(() => shipmentService.FilterByWarehouse(-1));

            }
        }

        [TestMethod]
        public void FilterByWarehouse_ShouldReturnCorrect_WhenIdIsFound()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                int filterWarehouseId = (int)context.Shipments.First().WarehouseId;

                // Act
                var filteredShipments = shipmentService.FilterByWarehouse(filterWarehouseId);

                // Assert
                foreach (var shipment in filteredShipments)
                {
                    Assert.AreEqual(filterWarehouseId, shipment.DestinationWarehouse.Id);
                }
            }
        }

        [TestMethod]
        public void FilterByWarehouse_ShouldReturnAllShipments_WhenNoFilterCriteria()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                ShipmentFilteringCriterias filter = new ShipmentFilteringCriterias() { Street = null, City = null, Country = null };

                // Act
                var filteredShipments = shipmentService.FilterByWarehouse(filter);

                // Assert
                Assert.AreEqual(context.Shipments.Count(), filteredShipments.Count());
                Assert.AreEqual(context.Shipments.First().ShipmentId, filteredShipments.First().Id);
                Assert.AreEqual(context.Shipments.Last().ShipmentId, filteredShipments.Last().Id);
            }

        }


        [TestMethod]
        public void FilterByWarehouse_ShouldReturnCorrect()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));
                
                Address address = context.Addresses
                    .Include(a => a.City)
                        .ThenInclude(c => c.Country)
                    .First();

                Warehouse warehouse = context.Warehouses.FirstOrDefault(w => w.Address == address);

                int count = context.Shipments.Where(sh => sh.WarehouseId == warehouse.WarehouseId).Count();

                List<ShipmentFilteringCriterias> filters = new List<ShipmentFilteringCriterias>()
                {
                    new ShipmentFilteringCriterias(){Street = address.Street, City = null, Country = null},
                    new ShipmentFilteringCriterias(){Street = null, City = address.City.Name, Country = null},
                    new ShipmentFilteringCriterias(){Street = null, City = null, Country = address.City.Country.Name}
                };

                foreach (var filter in filters)
                {
                    // Act
                    var filteredShipments = shipmentService.FilterByWarehouse(filter);

                    // Assert
                    Assert.AreEqual(count, filteredShipments.Count());
                    Assert.AreEqual(warehouse.WarehouseId, filteredShipments.First().DestinationWarehouse.Id);
                    Assert.AreEqual(warehouse.WarehouseId, filteredShipments.Last().DestinationWarehouse.Id);
                }
            }

        }
    }
}
