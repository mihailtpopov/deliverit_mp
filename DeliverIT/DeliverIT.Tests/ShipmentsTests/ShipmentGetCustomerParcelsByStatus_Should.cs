﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ShipmentsTests
{
    [TestClass]
    public class ShipmentGetCustomerParcelsByStatus_Should
    {
        [TestMethod]
        public void GetCustomerParcelsByStatus_ShouldThrow_WhenInvalidCustomerId()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                //Act && Assert
                Assert.ThrowsException<CustomerNotFoundException>(() => shipmentService.GetCustomerParcelsByStatus(-1, ""));
            }
        }

        [TestMethod]
        public void GetCustomerParcelsByStatus_ShouldThrow_WhenInvalidStatus()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                //Act && Assert
                Assert.ThrowsException<ShipmentStatusNotFoundException>(() => shipmentService.GetCustomerParcelsByStatus(1, ""));
            }
        }

        [TestMethod]
        public void GetCustomerParcelsByStatus_ShouldReturnCorrectly_WhenValidInput()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                Util.SeedDataInMemoryDatabase(context);

                // Act
                IEnumerable<ParcelDTO> parcels1 = shipmentService.GetCustomerParcelsByStatus(1,"preparing");

                IEnumerable<ParcelDTO> parcels2 = shipmentService.GetCustomerParcelsByStatus(1,"on the way");

                // Assert
                Assert.AreEqual(2, parcels1.Count());
                Assert.AreEqual(1, parcels1.First().Id);

                Assert.AreEqual(0, parcels2.Count());

            }
        }

    }
}
