﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ShipmentsTests
{
    [TestClass]
    public class ShipmentCheckShipmentStatus_Should
    {
        [TestMethod]
        public void CheckShipmentStatus_Should_Throw_WhenInvalidId()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                //Act && Assert
                Assert.ThrowsException<ShipmentNotFoundException>(() => shipmentService.CheckStatus(-1));
            }

        }

        [TestMethod]
        public void CheckShipmentStatus_Should_ReturnCorrectly_WhenValidId()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                //Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                List<Shipment> shipments = new List<Shipment>()
                {
                    new Shipment(){ShipmentStatusId = 2 },
                    new Shipment(){ShipmentStatusId = 1 }
                };

                context.Database.EnsureDeleted();
                context.ShipmentStatuses.AddRange(Util.SeedShipmentStatuses(context));
                context.Shipments.AddRange(shipments);
                context.SaveChanges();


                // Act
                string status1 = shipmentService.CheckStatus(1);
                string status2 = shipmentService.CheckStatus(2);

                // Assert
                string expectedStatus1 = context.ShipmentStatuses.FirstOrDefault(s => s.ShipmentStatusId == 2).Status;
                string expectedStatus2 = context.ShipmentStatuses.FirstOrDefault(s => s.ShipmentStatusId == 1).Status;

                Assert.AreEqual(expectedStatus1, status1);
                Assert.AreEqual(expectedStatus2, status2);
            }
        }

    }
}
