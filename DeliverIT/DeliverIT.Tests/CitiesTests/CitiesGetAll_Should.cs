﻿using DeliverIT.Data;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.CitiesTests
{
    [TestClass]
    public class CitiesGetAll_Should
    {
        [TestMethod]
        public void GetAll_ShouldReturnCorrectType()
        {
            //Arrange
            var options = Util.GetDbContextOptions(nameof(GetAll_ShouldReturnCorrectType));

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var cityService = new CityService(context);

                // Act
                var citiesDb = cityService.GetAll().ToList();

                //Assert
                Assert.IsInstanceOfType(citiesDb, typeof(IEnumerable<CityDTO>));
            }
        }

        [TestMethod]
        public void GetAll_ShouldReturnSameCollectionAsExcepted()
        {
            //Arrange
            var options = Util.GetDbContextOptions(nameof(GetAll_ShouldReturnSameCollectionAsExcepted));

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var citites = context.Cities.ToList();

                var cityService = new CityService(context);

                // Act
                var citiesDb = cityService.GetAll().ToList();

                //Assert
                Assert.AreEqual(citites.Count, citiesDb.Count);
                Assert.AreEqual(citites.First().CityId, citiesDb.First().Id);
                Assert.AreEqual(citites.Last().CityId, citiesDb.Last().Id);
            }
        }
    }
}
