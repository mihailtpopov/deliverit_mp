﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Tests.CountriesTests
{
    [TestClass]
    public class CountriesGetById_Should
    {
        [TestMethod]
        public void GetById_ShouldThrowCountryNotFoundException_WhenInvalidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                //Act && Assert
                var countryService = new CountryService(context);

                Assert.ThrowsException<CountryNotFoundException>(() => countryService.Get(9));
            }
        }

        [TestMethod]
        public void GetById_ShouldReturnCountriesDTO_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var countryService = new CountryService(context);

                //Act
                var country = countryService.Get(1);

                //Assert
                Assert.IsInstanceOfType(country, typeof(CountryDTO));
            }
        }

        [TestMethod]
        public void GetById_ShoudlReturnCorrectCountry_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var countryService = new CountryService(context);

                //Act
                var country = countryService.Get(1);

                //Assert
                Assert.AreEqual(1, country.Id);
            }
        }
    }
}
