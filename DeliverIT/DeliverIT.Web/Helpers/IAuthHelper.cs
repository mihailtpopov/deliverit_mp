﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIT.Web.Helpers
{
    public interface IAuthHelper
    {
        Customer TryGetCustomer(string email);
        Employee TryGetEmployee(string email);
    }
}
