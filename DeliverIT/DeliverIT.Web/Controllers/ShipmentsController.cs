﻿using DeliverIT.Data.Models;
using DeliverIT.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Exceptions;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Web.Helpers;
using DeliverIT.Services.QueryObjects;
using DeliverIT.Services.Models.OutputDTOs;
using DeliverIT.Exceptions.Messages;
using DeliverIT.Exceptions.Strings;

namespace DeliverIT.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Route("api/[controller]")]
    [ApiController]
    public class ShipmentsController : Controller
    {
        /// <summary>
        /// The shipment service
        /// </summary>
        private readonly IShipmentService shipmentService;
        /// <summary>
        /// The authentication helper
        /// </summary>
        private readonly IAuthHelper authHelper;
        private readonly ICustomerService customerService;
        private readonly IEmployeeService employeeService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShipmentsController"/> class.
        /// </summary>
        /// <param name="shipmentService">The shipment service.</param>
        /// <param name="authHelper">The authentication helper.</param>
        public ShipmentsController(IShipmentService shipmentService, IAuthHelper authHelper, ICustomerService customerService, IEmployeeService employeeService)
        {
            this.shipmentService = shipmentService;
            this.authHelper = authHelper;
            this.customerService = customerService;
            this.employeeService = employeeService;
        }


        /// <summary>
        /// Gets all shipments.
        /// </summary>
        /// <param name="filter">The pagination filter.</param>
        /// <param name="authentication">The user authentication.</param>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult GetAll([FromQuery] PaginationFilter filter, [FromHeader] string authentication)
        {

            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var result = shipmentService.GetAll();
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                return Ok(new PagedResponse<ShipmentDTO>().GetPaginated(result, validFilter.PageNumber, validFilter.PageSize));
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
        }

        /// <summary>
        /// Gets a shipment by specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="authentication">The user authentication.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult Get(int id, [FromHeader] string authentication)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                return Ok(shipmentService.Get(id));
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
            catch (ShipmentNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Creates e new shipment.
        /// </summary>
        /// <param name="shipment">The new shipment.</param>
        /// <param name="authentication">The user authentication.</param>
        /// <returns></returns>
        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult Create([FromBody] InputShipment shipment, [FromHeader] string authentication)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var createdShipment = shipmentService.Create(shipment);
                return Created("uri?", createdShipment);
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
        }

        /// <summary>Updates the specified shipment.</summary>
        /// <param name="id">The shipment identifier.</param>
        /// <param name="updateShipment">The update shipment.</param>
        /// <param name="authentication">The user authentication.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult Update(int id, [FromBody] InputShipment updateShipment, [FromHeader] string authentication)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);
                
                return Ok(shipmentService.Update(id, updateShipment));
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
            catch (ShipmentNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (ShipmentStatusException e)
            {
                return BadRequest(e.Message);
            }
            catch (ShipmentStatusNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Deletes the specified shipment.
        /// </summary>
        /// <param name="id">The shipment identifier.</param>
        /// <param name="authentication">The user authentication.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult Delete(int id, [FromHeader] string authentication)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                shipmentService.Delete(id);
                return NoContent();
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
            catch (ShipmentNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Adds a parcel to a shipment.
        /// </summary>
        /// <param name="shipmentId">The shipment identifier.</param>
        /// <param name="parcelId">The parcel identifier.</param>
        /// <param name="authentication">The user authentication.</param>
        /// <returns></returns>
        [HttpPost("{shipmentId}/parcels/{parcelId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult AddParcel(int shipmentId, int parcelId, [FromHeader] string authentication)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                shipmentService.AddParcel(shipmentId, parcelId);
                return Ok(Strings.PARCEL_ADDED);
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
            catch (ParcelNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (ShipmentNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Removes a parcel from a shipment.
        /// </summary>
        /// <param name="shipmentId">The shipment identifier.</param>
        /// <param name="parcelId">The parcel identifier.</param>
        /// <param name="authentication">The user authentication.</param>
        /// <returns></returns>
        [HttpDelete("{shipmentId}/parcels/{parcelId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult RemoveParcel(int shipmentId, int parcelId, [FromHeader] string authentication)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                shipmentService.RemoveParcel(shipmentId, parcelId);
                return Ok(Strings.PARCEL_REMOVED);
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
            catch (ParcelNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (ShipmentNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

        }

        /// <summary>
        /// Gets the shipments count that are "on the way".
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <returns></returns>
        [HttpGet("ontheway")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult GetOnTheWayCount([FromHeader] string authentication)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var count = shipmentService.GetOnWayCount();
                return Ok(count);

            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
        }

        /// <summary>
        /// Checks the status of a specified shipment.
        /// </summary>
        /// <param name="id">The shipment identifier.</param>
        /// <param name="authentication">The user authentication.</param>
        /// <returns></returns>
        [HttpGet("status/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult GetShipmentStatus(int id, [FromHeader] string authentication)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                string status = shipmentService.CheckStatus(id);
                return Ok(status);
            }
            catch (ShipmentNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
        }

        /// <summary>Gets the shipment status by customer parcel.</summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="parcelId">The parcel identifier.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="authentication">The authentication.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        [HttpGet("statuses/{customerId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult GetShipmentStatusByCustomerParcel(int customerId, [FromQuery] int parcelId, [FromQuery] PaginationFilter filter, [FromHeader] string authentication)
        {

            try
            {
                string status = null;

                if (employeeService.IsAuthenticated(authentication))
                {
                    status = shipmentService.CheckShipmentStatusByParcel(parcelId, customerId);
                    if (status == null)
                    {
                        return NoContent();
                    }
                    return Ok(status);
                }

                var customer = authHelper.TryGetCustomer(authentication);
                if (customerId != customer.CustomerId)
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                else
                {
                    status = shipmentService.CheckShipmentStatusByParcel(parcelId, customerId);
                    if (status == null)
                    {
                        return NoContent();
                    }
                    return Ok(status);
                }
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
            catch (CustomerNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (ShipmentStatusNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Gets the customer's parcels wth the specified status.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="filter">The pagination filter.</param>
        /// <param name="authentication">The user authentication.</param>
        /// <returns></returns>
        [HttpGet("parcels/{customerId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult GetCustomerParcelsByStatus([FromQuery] string status, int customerId, [FromQuery] PaginationFilter filter, [FromHeader] string authentication)
        {
            try
            {
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);

                IEnumerable<ParcelDTO> parcels = new List<ParcelDTO>();

                if (employeeService.IsAuthenticated(authentication))
                {
                    parcels = shipmentService.GetCustomerParcelsByStatus(customerId, status);
                    if (!parcels.Any())
                    {
                        return NoContent();
                    }

                    return Ok(new PagedResponse<ParcelDTO>().GetPaginated(parcels, validFilter.PageNumber, validFilter.PageSize));
                }

                var customer = authHelper.TryGetCustomer(authentication);
                if (customerId != customer.CustomerId)
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                else
                {
                    parcels = shipmentService.GetCustomerParcelsByStatus(customerId, status);

                    if (!parcels.Any())
                    {
                        return NoContent();
                    }
                    return Ok(new PagedResponse<ParcelDTO>().GetPaginated(parcels, validFilter.PageNumber, validFilter.PageSize));
                }

            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
            catch (CustomerNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (ShipmentStatusNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Gets the next shipment that will arrive in specified warehouse.
        /// </summary>
        /// <param name="warehouseId">The warehouse identifier.</param>
        /// <param name="authentication">The user authentication.</param>
        /// <returns></returns>
        [HttpGet("next")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetNextByWarehouse([FromQuery] int warehouseId, [FromHeader] string authentication)
        {
            try
            {
                if(customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var nextShipment = shipmentService.GetNextByWarehouse(warehouseId);
                return Ok(nextShipment);
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
            catch (WarehouseNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (ShipmentNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Gets the shipments by warehouse.
        /// </summary>
        /// <param name="id">The warehouse identifier.</param>
        /// <param name="filter">The pagination filter.</param>
        /// <param name="authentication">The user authentication.</param>
        /// <returns></returns>
        [HttpGet("warehouses/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetByWarehouse(int id, [FromQuery] PaginationFilter filter, [FromHeader] string authentication)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var shipments = shipmentService.FilterByWarehouse(id);

                if (!shipments.Any())
                {
                    return NoContent();
                }

                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                return Ok(new PagedResponse<ShipmentDTO>().GetPaginated(shipments, validFilter.PageNumber, validFilter.PageSize));
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
            catch (WarehouseNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Gets the shipments by specified parts of warehouse address.
        /// </summary>
        /// <param name="criterias">The warehouse address filter criterias (street, city, country).</param>
        /// <param name="filter">The pagination filter.</param>
        /// <param name="authentication">The user authentication.</param>
        /// <returns></returns>
        [HttpGet("warehouses")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult GetByWarehouse([FromQuery] ShipmentFilteringCriterias criterias, [FromQuery] PaginationFilter filter, [FromHeader] string authentication)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var shipments = shipmentService.FilterByWarehouse(criterias);

                if (!shipments.Any())
                {
                    return NoContent();
                }

                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                return Ok(new PagedResponse<ShipmentDTO>().GetPaginated(shipments, validFilter.PageNumber, validFilter.PageSize));
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }        
        }

        /// <summary>
        /// Gets the shipments by customer criterias.
        /// </summary>
        /// <param name="criterias">The customer criterias.</param>
        /// <param name="filter">The pagination filter</param>
        /// <param name="authentication">The authentication.</param>
        /// <returns></returns>
        [HttpGet("customers")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult GetByCustomer([FromQuery] CustomerFilteringCriterias criterias, [FromQuery] PaginationFilter filter, [FromHeader] string authentication)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }

                authHelper.TryGetEmployee(authentication);

                var shipments = shipmentService.FilterByCustomer(criterias);

                if (!shipments.Any())
                {
                    return NoContent();
                }

                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                return Ok(new PagedResponse<ShipmentDTO>().GetPaginated(shipments, validFilter.PageNumber, validFilter.PageSize));
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
        }
    }
}
