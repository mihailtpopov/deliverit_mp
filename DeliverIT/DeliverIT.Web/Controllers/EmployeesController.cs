﻿using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Messages;
using DeliverIT.Services.Interfaces;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using DeliverIT.Services.QueryObjects;
using DeliverIT.Web.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIT.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        /// <summary>
        /// The employee service
        /// </summary>
        public IEmployeeService employeeService;
        /// <summary>
        /// The authentication helper
        /// </summary>
        private readonly IAuthHelper authHelper;
        private readonly ICustomerService customerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeesController"/> class.
        /// </summary>
        /// <param name="employeeService">The employee service.</param>
        /// <param name="authHelper">The authentication helper.</param>
        public EmployeesController(IEmployeeService employeeService, IAuthHelper authHelper, ICustomerService customerService)
        {
            this.employeeService = employeeService;
            this.authHelper = authHelper;
            this.customerService = customerService;
        }

        /// <summary>
        /// Gets all employees.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="filter">The pagination filter</param>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult Get([FromHeader] string authentication, [FromQuery] PaginationFilter filter)
        {
            try
            {

                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var allEmployees = employeeService.GetAll();

                if(!allEmployees.Any())
                {
                    return NoContent();
                }

                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                return Ok(new PagedResponse<EmployeeDTO>().GetPaginated(allEmployees, validFilter.PageNumber, validFilter.PageSize));
            }
            catch (ArgumentException ae)
            {
                return NotFound(ae.Message);
            }
            catch(UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Gets employee by specified identifier.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult Get([FromHeader] string authentication, int id)
        {
            try
            {
                if(customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var customer = employeeService.Get(id);
                return Ok(customer);
            }
            catch (EmployeeNotFoundException enfe)
            {
                return NotFound(enfe.Message);
            }
            catch(UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Register the specified employee.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="employee">The employee to create.</param>
        /// <returns></returns>
        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]

        public IActionResult Post([FromHeader] string authentication, [FromBody] InputEmployee employee)
        {
            try
            {
                if(customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var createdCustomer = employeeService.Create(employee);
                return Created("uri", createdCustomer);
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch(UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Updates employee by specified identifier.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="employee">The input employee.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Put([FromHeader] string authentication, int id, [FromBody] InputEmployee employee)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var updatedCustomer = employeeService.Update(id, employee);
                return Ok(updatedCustomer);
            }
            catch(ArgumentNullException ane)
            {
                return BadRequest(ane.Message);
            }
            catch (EmployeeNotFoundException enfe)
            {
                return NotFound(enfe.Message);
            }
            catch(AddressNotFoundException ane)
            {
                return BadRequest(ane.Message);
            }
            catch(DuplicateEmailAddressException deae)
            {
                return BadRequest(deae.Message);
            }
            catch(UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Deletes the employee by specified identifier.
        /// </summary>
        /// <param name="authencation">The user authentication.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult Delete([FromHeader] string authencation, int id)
        {
            try
            {
                if(customerService.IsAuthenticated(authencation))
                {
                     return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authencation);

                var isDeleted = employeeService.Delete(id);

                if (isDeleted)
                {
                    return NoContent();
                }

                return NotFound();
            }
            catch(UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }
    }
}
