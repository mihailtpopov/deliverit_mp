﻿using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Messages;
using DeliverIT.Services.Interfaces;
using DeliverIT.Services.Models.OutputDTOs;
using DeliverIT.Services.QueryObjects;
using DeliverIT.Web.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace DeliverIT.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("api/[controller]")]
    [ApiController]
    public class CitiesController : ControllerBase
    {
        /// <summary>
        /// The city service
        /// </summary>
        private readonly ICityService cityService;
        /// <summary>
        /// The authentication helper
        /// </summary>
        private readonly IAuthHelper authHelper;
        private readonly ICustomerService customerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CitiesController"/> class.
        /// </summary>
        /// <param name="cityService">The city service.</param>
        /// <param name="authHelper">The authentication helper.</param>
        public CitiesController(ICityService cityService, IAuthHelper authHelper, ICustomerService customerService)
        {
            this.cityService = cityService;
            this.authHelper = authHelper;
            this.customerService = customerService;
        }

        /// <summary>
        /// Gets all cities.
        /// </summary>
        /// <param name="filter">The pagination filter</param>
        /// <param name="authentication">The user authentication.</param>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult GetAll([FromQuery] PaginationFilter filter, [FromHeader] string authentication)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                this.authHelper.TryGetEmployee(authentication);

                var allCities = this.cityService.GetAll();

                if (!allCities.Any())
                {
                    return NoContent();
                }

                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                return Ok(new PagedResponse<CityDTO>().GetPaginated(allCities, validFilter.PageNumber, validFilter.PageSize));
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }        
        }

        /// <summary>
        /// Gets the specified city.
        /// </summary>
        /// <param name="id">The city identifier.</param>
        /// <param name="authentication">The user authentication.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult Get(int id, [FromHeader] string authentication)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                this.authHelper.TryGetEmployee(authentication);

                var city = this.cityService.Get(id);
                return Ok(city);
            }
            catch (CityNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
        }

    }
}
