﻿using DeliverIT.Exceptions.Messages;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeliverIT.Data.Models
{
    /// <author>
    /// Georgi Stefanov
    /// </author>
    public class Shipment
    {
        public Shipment()
        {
            this.Parcels = new List<Parcel>();
        }

        [Key]
        public int ShipmentId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DepartureDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ArrivalDate { get; set; }

        //[ForeignKey("ShipmentStatusId")]
        [Required(ErrorMessage = ErrorMessages.SHIPMENT_STATUS_ID_REQUIRED)]
        public int ShipmentStatusId { get; set; }

        public ShipmentStatus ShipmentStatus { get; set; }

        public int? WarehouseId { get; set; }

        public Warehouse Warehouse { get; set; }

        public ICollection<Parcel> Parcels { get; set; }
    }
}
