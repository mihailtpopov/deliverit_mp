﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
   public class AddressNotFoundException : Exception
    {
        public AddressNotFoundException(int id) 
            : base(string.Format(ErrorMessages.NOT_FOUND,"Address", "id", id))
        {

        }
    }
}
