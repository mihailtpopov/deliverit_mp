﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class ShipmentNotFoundException : Exception
    {
        public ShipmentNotFoundException(string message)
            : base(message)
        {

        }

        public ShipmentNotFoundException(int id)
            : base(string.Format(ErrorMessages.NOT_FOUND, "Shipment", "id", id))
        {

        }
    }
}
