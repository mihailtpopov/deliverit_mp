﻿using DeliverIT.Exceptions.Messages;
using System;

namespace DeliverIT.Exceptions
{
    public class EmployeeNotFoundException : Exception
    {
        public EmployeeNotFoundException(int id)
            : base(string.Format(ErrorMessages.NOT_FOUND,"Employee", "id", id))
        {

        }
    }
}
