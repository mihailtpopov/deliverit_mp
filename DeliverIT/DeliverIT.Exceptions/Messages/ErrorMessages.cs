﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions.Messages
{
    public static class ErrorMessages
    {
        public const string FAILED_REGISTRATION = "Registration failed. Please provide valid {0}.";
        public const string FAILED_UPDATE = "Update failed. Please provide valid {0}.";
        public const string FAILED_UPDATE_2 = "Update failed. {0}.";
        public const string NOT_FOUND = "{0} with {1} {2} is not found!";

        //customer && employee error messages
        public const string INVALID_EMAIL_FORMAT = "Wrong email address format. Please provide valid email format";
        public const string FIRST_NAME_REQUIRED = "First name is required!";
        public const string LAST_NAME_REQUIRED = "Last name is required!";
        public const string EMAIL_ADDRESS_REQUIRED = "Email address is required!";
        public const string ADDRESS_ID_REQUIRED = "Address id is required!";
        public const string DUPLICATE_EMAIL_ADDRESS = "{0} with email address \"{1}\" already exists!";
        public const string INVALID_CREDENTIALS = "Invalid credentials";

        //countries error messages
        public const string NO_CITIES_IN_COUNTRY = "No cities found in this country!";

        //parcel error messages
        public const string NEGATIVE_WEIGHT = "Weight can't be negative!";
        public const string CATEGORY_ID_REQUIRED = "Category id is required!";
        public const string WEIGHT_REQUIRED = "Weight is required!";
        public const string CUSTOMER_ID_REQUIRED = "Purchaser id is required!";

        //common error messages
        public const string PARAMETER_REQUIRED = "{0} is required!";
        public const string INVALID_ID = "{0} with id {1} can not be found!";
        public const string FORBIDDEN_ACCESS = "Forbidden user access!";

        //address error messages
        public const string STREET_REQUIRED = "Street is required!";
        public const string INVALID_CITY_ID = "Invalid city id!";

        //category error messages
        public const string CATEGORY_NAME_REQUIRED = "Category name is required!";

        //city error messages
        public const string CITY_NAME_REQUIRED = "City name is required!";
        public const string CITY_ID_REQUIRED = "City id is required!";
        public const string COUNTRY_ID_REQUIRED = "Country id is required!";

        //country error messages
        public const string COUTNRY_NAME_REQUIRED = "Country name is required.";

        //shipment error messages
        public const string SHIPMENT_STATUS_ID_REQUIRED = "Shipment status id is required.";
        public const string PARCEL_ADD_REMOVE_IMPOSSIBLE = @"Can't {0} parcel. The shipment is either ""on the way"" or ""completed""!";

        //shipment status error messages
        public const string SHIPMENT_STATUS_NAME_REQUIRED = "Status name is required!";
    }
}
