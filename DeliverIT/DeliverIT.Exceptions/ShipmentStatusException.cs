﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class ShipmentStatusException : Exception
    {
        public ShipmentStatusException(string message)
            : base(message)
        {

        }
    }
}
