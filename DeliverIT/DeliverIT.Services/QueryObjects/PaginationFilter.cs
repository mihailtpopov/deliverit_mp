﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.QueryObjects
{
    public class PaginationFilter
    {
        public PaginationFilter()
        {
           
        }

        public PaginationFilter(int pageNumber, int pageSize)
        {
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = (pageSize > 3 || pageSize < 1) ? 3 : pageSize;
        }

        public int PageNumber { get; set; }

        public int PageSize { get; set; }
    }
}
