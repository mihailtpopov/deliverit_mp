﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Messages;
using DeliverIT.Services.Interfaces;
using DeliverIT.Services.Models;
using DeliverIT.Services.Models.OutputDTOs;
using DeliverIT.Services.QueryObjects;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Services
{
    public class CountryService : ICountryService
    {
        /// <summary>
        /// The database context
        /// </summary>
        private IDeliverITDbContext dbContext;
        /// <summary>
        /// Initializes a new instance of the <see cref="CountryService"/> class.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        public CountryService(IDeliverITDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        /// <summary>
        /// Gets all countries.
        /// </summary>
        /// <returns>Collection of all countries as CountryDTO objects</returns>
        public IEnumerable<CountryDTO> GetAll()
        {
            var countries = dbContext.Countries.Include(c => c.Cities);

            return GetCountryDTOs(countries);
        }

        /// <summary>
        /// Gets country by specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The country to get as CountryDTO object</returns>
        /// <exception cref="CountryNotFoundException"></exception>
        public CountryDTO Get(int id)
        {
            var country = GetIncludedCountries()
                .FirstOrDefault(c => c.CountryId == id);

            if(country == null)
            {
                throw new CountryNotFoundException(id);
            }

            return new CountryDTO(country);
        }

        /// <summary>
        /// Gets country by name.
        /// </summary>
        /// <param name="name">The country name.</param>
        /// <returns>The country to get as CountryDTO object</returns>
        /// <exception cref="CountryNotFoundException"></exception>
        public CountryDTO GetByName(string name)
        {
            var country = GetIncludedCountries()
                .FirstOrDefault(c => c.Name == name);

            if (country == null)
            {
                throw new CountryNotFoundException(name);
            }

            return new CountryDTO(country);
        }

        /// <summary>
        /// Gets country cities by specified country name.
        /// </summary>
        /// <param name="countryName">Name of the country.</param>
        /// <returns>Country cities as CountryCitiesDTO object</returns>
        /// <exception cref="CountryNotFoundException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public CountryCitiesDTO GetCities(string countryName)
        {
            var country = dbContext.Countries
                .Include(c => c.Cities)
                .FirstOrDefault(c => c.Name == countryName);

            if(country == null)
            {
                throw new CountryNotFoundException(countryName);
            }

            if(!country.Cities.Any())
            {
                throw new ArgumentException(ErrorMessages.NO_CITIES_IN_COUNTRY);
            }

            return new CountryCitiesDTO(country);
        }

        /// <summary>
        /// Gets the country DTOs.
        /// </summary>
        /// <param name="countries">The countries.</param>
        /// <returns>Collection of countries passed as CountryDTO objects</returns>
        private IEnumerable<CountryDTO> GetCountryDTOs(IEnumerable<Country> countries)
        {
            List<CountryDTO> countryDTOs = new List<CountryDTO>();

            foreach (var country in countries)
            {
                countryDTOs.Add(new CountryDTO(country));
            }

            return countryDTOs;
        }

        /// <summary>
        /// Gets the countries with their included relations.
        /// </summary>
        /// <returns>Collection of the countries</returns>
        private IEnumerable<Country> GetIncludedCountries()
        {
            var countries = dbContext.Countries.Include(c => c.Cities);

            return countries;
        }
    }
}
