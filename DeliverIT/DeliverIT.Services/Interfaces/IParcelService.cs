﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Models;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IParcelService
    {
        IEnumerable<ParcelDTO> GetAll();

        ParcelDTO Get(int id);

        ParcelDTO Create(InputParcel parcel);

        ParcelDTO Update(int id, InputParcel parcel);

        bool Delete(int id);

        IEnumerable<ParcelDTO> FilterByCustomer(string firstName, string lastName);

        IEnumerable<ParcelDTO> FilterByWarehouse(string city, string country);

        IEnumerable<ParcelDTO> FilterByCategory(string categoryName);

        IEnumerable<ParcelDTO> FilterByWeight(double toWeight, double fromWeight = 0);

        IEnumerable<ParcelDTO> SortBy(string sortCriteria);
    }
}
