﻿using DeliverIT.Data.Models;
using DeliverIT.Services.Models;
using DeliverIT.Services.Models.OutputDTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Interfaces
{
    public interface ICityService
    {
        IEnumerable<CityDTO> GetAll();

        CityDTO Get(int id);

    }
}
