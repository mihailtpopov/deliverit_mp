﻿using DeliverIT.Data.Models;
using DeliverIT.Services.Models;
using DeliverIT.Services.Models.OutputDTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Interfaces
{
    public interface ICountryService
    {
        IEnumerable<CountryDTO> GetAll();

        CountryDTO Get(int id);

        CountryDTO GetByName(string name);

        CountryCitiesDTO GetCities(string countrName);
    }
}
