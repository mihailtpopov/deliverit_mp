﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DeliverIT.Services.Models.InputDTOs
{
    public class InputEmployee
    {
        [StringLength(25, MinimumLength = 2, ErrorMessage = "First name should be between {2} and {1} symbols")]
        public string FirstName { get; set; }

        [StringLength(25, MinimumLength = 2, ErrorMessage = "First name should be between {2} and {1} symbols")]
        public string LastName { get; set; }

        [EmailAddress(ErrorMessage = ErrorMessages.INVALID_EMAIL_FORMAT)]
        public string Email { get; set; }

        public int? AddressId { get; set; }
    }
}
