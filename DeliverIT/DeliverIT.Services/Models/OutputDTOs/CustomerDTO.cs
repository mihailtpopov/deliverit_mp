﻿using DeliverIT.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace DeliverIT.Services.Models.OutputDTOs
{
    public class CustomerDTO
    {
        public CustomerDTO(Customer customer)
        {
            this.Name = customer.FirstName + " " + customer.LastName;
            this.Email = customer.Email;
            this.Address = customer.Address.Street +
                ", " + customer.Address.City.Name +
                ", " + customer.Address.City.Country.Name;
            this.Id = customer.CustomerId;
        }

        [JsonIgnore]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }
}
