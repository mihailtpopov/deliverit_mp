﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliverIT.Exceptions;
using Microsoft.EntityFrameworkCore;
using DeliverIT.Services.Models;
using DeliverIT.Services.Models.OutputDTOs;

namespace DeliverIT.Services
{
    /// <summary>
    /// Warehouse Service
    /// </summary>
    /// <seealso cref="DeliverIT.Services.Interfaces.IWarehouseService" />
    public class WarehouseService : IWarehouseService

    {
        /// <summary>
        /// The database context
        /// </summary>
        private readonly IDeliverITDbContext dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="WarehouseService"/> class.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        public WarehouseService(IDeliverITDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Gets the specified warehouse.
        /// </summary>
        /// <param name="id">The warehouse identifier.</param>
        /// <returns></returns>
        /// <exception cref="WarehouseNotFoundException"></exception>
        public WarehouseDTO Get(int id)
        {
            var warehouse = dbContext.Warehouses
                .Include(w => w.Address)
                    .ThenInclude(a => a.City)
                        .ThenInclude(c => c.Country)
                .FirstOrDefault(w => w.WarehouseId == id)
                ?? throw new WarehouseNotFoundException(id);

            WarehouseDTO warehouseDTO = new WarehouseDTO(warehouse);

            return warehouseDTO;
        }

        /// <summary>
        /// Gets all warehouses.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<WarehouseDTO> GetAll()
        {
            var allWarehouses = dbContext.Warehouses
                .Include(w => w.Address)
                    .ThenInclude(a => a.City)
                        .ThenInclude(c => c.Country)
                .Select(w => new WarehouseDTO(w));

            return allWarehouses;
        }
    }
}
